// tạo mảng chứa danh sách nhân viên
var dsnvArr = [];
// tạo hàm lưu dánh sách xuống local
const DSNV = "DSNV";
function luuLocal(){
    var dsnvArrJson = JSON.stringify(dsnvArr);
    localStorage.setItem(DSNV, dsnvArrJson);
}

// lấy danh sách nhân vien dưới local in lại lên giao diện
var dataNV = localStorage.getItem(DSNV);
if(dataNV !== null){
    // convert thành array
    var dataArr = JSON.parse(dataNV);
    // duyệt mảng map lại để có function
    for(var i = 0; i < dataArr.length; i++){
        var data = dataArr[i];
        var newData = new NhanVien(
            data.taiKhoan,
            data.ten,
            data.email,
            data.pass,
            data.ngay,
            data.gio,
            data.chuc,
            data.luong
        )
        // push newData vào lại mảng nhân viên ban đầu
        dsnvArr.push(newData);
    }
    // in lại lên giao diện
    inDSNV(dsnvArr)
}

// thêm nhân viên
document.getElementById("btnThemNV").onclick = function() {
    // input: lay thong tin nhan vien ~ viet ham lay thong tin
    var thongTinNV = layThongTin();

    // kiểm tra người dùng nhập thông tin
    var isValid = true;
    // kiểm tra tài khoản
    isValid =
    isValid &
    validator.kiemTraRong(thongTinNV.taiKhoan, "tbTKNV", "Tài khoản không được để trống")&&
    validator.kiemTraTrung(thongTinNV.taiKhoan, dsnvArr)&&
    validator.kiemTraDoDai(thongTinNV.taiKhoan, "tbTKNV", "Tài khoản tối đa 4-6 ký số",4 ,6);
    // kiểm tra tên
    isValid = 
    isValid &
    validator.kiemTraRong(thongTinNV.ten, "tbTen", "Tên nhân viên không được để trống")&&
    validator.kiemTraLaChuSo(thongTinNV.ten, "tbTen", "Tên nhân viên phải là chữ");
    
    // kiểm tra email
    isValid =
    isValid &
    validator.kiemTraRong(thongTinNV.email, "tbEmail", "Email không được để trống")&&
    validator.kiemTraEmail(thongTinNV.email, "tbEmail", "Email không đúng định dạng");

    // kiểm tra mật khẩu
    isValid = 
    isValid &
    validator.kiemTraRong(thongTinNV.pass, "tbMatKhau", "Mật khẩu không được đẻ trống")&&
    validator.kiemTraDoDai(thongTinNV.pass, "tbMatKhau", "Mật khẩu có 6-8 ký tự", 6, 8)&&
    validator.kiemTraMatKhau(thongTinNV.pass, "tbMatKhau", "Mật khẩu có ít nhất 1 ký tự số, 1 ký tự in hoa và 1 ký tự đặc biệt");

    // kiểm tra ngày
    isValid = 
    isValid &
    validator.kiemTraRong(thongTinNV.ngay, "tbNgay", "Ngày không được để trống")&&
    validator.kiemTraNgay(thongTinNV.ngay, "tbNgay", "Nhập ngày theo định dạng mm/dd/yyyy");

    // kiểm tra lương
    isValid =
    isValid &
    validator.kiemTraRong(thongTinNV.luong, "tbLuongCB", "Lương không được để trống")&&
    validator.kiemTraMinMax(thongTinNV.luong, "tbLuongCB", "Lương cơ bản thấp nhất là 1 triệu và cao nhất là 20 triệu", 1000000, 20000000);

    // kiểm tra chức vụ
    isValid =
    isValid &
    validator.kiemTraChucVu(thongTinNV.chuc, "tbChucVu", "Vui lòng chọn chức vụ");
    
    // kiểm tra giờ làm trong tháng
    isValid =
    isValid &
    validator.kiemTraRong(thongTinNV.gio, "tbGiolam", "Giờ làm không để trống")&&
    validator.kiemTraMinMax(thongTinNV.gio, "tbGiolam", "Giờ làm ít nhất 80 và tối đa 200", 80, 200)

    if(isValid){

        // push vào mảng nhân viên
        dsnvArr.push(thongTinNV);
    
        // tải lại trang mất thông tin ~ viết hàm lưu danh sách nhân viên nhập vào xuống local
        luuLocal();
    
        // in danh sách lên giao diện
        inDSNV(dsnvArr);
    }

   
}

// sửa nhân viên
function suaNV(taiKhoan){
    // tìm nhân viên theo tài khoản nhân viên ~ viết hàm tìm vị trí
    var viTri = timViTri(taiKhoan, dsnvArr);
    if(viTri == -1) return;
    if(viTri !== -1){
        var tknv = dsnvArr[viTri];
        // xuất thông tin nhân viên tìm được lên form
        xuatThongTin(tknv);
        // ngăn người  dùng sửa tài khoản
        document.getElementById("tknv").disabled = true;
    }
}

// xóa nhân viên
function xoaNV(taiKhoan){
    // tìm nhân viên theo tài khoản
    var viTri= timViTri(taiKhoan, dsnvArr);
    if(viTri == -1) return;
    if(viTri !== -1){
        
        dsnvArr.splice(viTri, 1)
        // in lại danh sách nhân viên sau xóa
        inDSNV(dsnvArr);
        luuLocal();
    }
}

// cập nhật sinh viên
document.getElementById("btnCapNhat").onclick = function() {
    // lấy thông tin của nhân viên cần cập nhật
    var thongTinNV = layThongTin();
    // kiểm tra nhập
    // kiểm tra người dùng nhập thông tin
     // kiểm tra người dùng nhập thông tin
     var isValid = true;
     // kiểm tra tài khoản
     isValid =
     isValid &
     validator.kiemTraRong(thongTinNV.taiKhoan, "tbTKNV", "Tài khoản không được để trống")&&
     validator.kiemTraDoDai(thongTinNV.taiKhoan, "tbTKNV", "Tài khoản tối đa 4-6 ký số",4 ,6);
     // kiểm tra tên
     isValid = 
     isValid &
     validator.kiemTraRong(thongTinNV.ten, "tbTen", "Tên nhân viên không được để trống")&&
     validator.kiemTraLaChuSo(thongTinNV.ten, "tbTen", "Tên nhân viên phải là chữ");
     
     // kiểm tra email
     isValid =
     isValid &
     validator.kiemTraRong(thongTinNV.email, "tbEmail", "Email không được để trống")&&
     validator.kiemTraEmail(thongTinNV.email, "tbEmail", "Email không đúng định dạng");
 
     // kiểm tra mật khẩu
     isValid = 
     isValid &
     validator.kiemTraRong(thongTinNV.pass, "tbMatKhau", "Mật khẩu không được đẻ trống")&&
     validator.kiemTraDoDai(thongTinNV.pass, "tbMatKhau", "Mật khẩu có 6-8 ký tự", 6, 8)&&
     validator.kiemTraMatKhau(thongTinNV.pass, "tbMatKhau", "Mật khẩu có ít nhất 1 ký tự số, 1 ký tự in hoa và 1 ký tự đặc biệt");
 
     // kiểm tra ngày
     isValid = 
     isValid &
     validator.kiemTraRong(thongTinNV.ngay, "tbNgay", "Ngày không được để trống")&&
     validator.kiemTraNgay(thongTinNV.ngay, "tbNgay", "Nhập ngày theo định dạng mm/dd/yyyy");
 
     // kiểm tra lương
     isValid =
     isValid &
     validator.kiemTraRong(thongTinNV.luong, "tbLuongCB", "Lương không được để trống")&&
     validator.kiemTraMinMax(thongTinNV.luong, "tbLuongCB", "Lương cơ bản thấp nhất là 1 triệu và cao nhất là 20 triệu", 1000000, 20000000);
 
     // kiểm tra chức vụ
     isValid =
     isValid &
     validator.kiemTraChucVu(thongTinNV.chuc, "tbChucVu", "Vui lòng chọn chức vụ");
     
     // kiểm tra giờ làm trong tháng
     isValid =
     isValid &
     validator.kiemTraRong(thongTinNV.gio, "tbGiolam", "Giờ làm không để trống")&&
     validator.kiemTraMinMax(thongTinNV.gio, "tbGiolam", "Giờ làm ít nhất 80 và tối đa 200", 80, 200)
 
     if(isValid){
         
         // tìm nhân viên cần cập nhật theo tài khoản nhân viên
         var viTri = timViTri(thongTinNV.taiKhoan, dsnvArr);
         if(viTri == -1) return;
         if(viTri !== -1){
             dsnvArr[viTri] = thongTinNV;
             // in danh sách sau sửa
             inDSNV(dsnvArr);
             luuLocal();
             // bỏ chặn nhập tài khoản
             document.getElementById("tknv").disabled = false;
             
             }
     }

    }

    // tìm nhân viên
    document.getElementById("btnTimNV").onclick = function() {
        var arrSearch = [];
    var input = document.getElementById('searchName');
    var value = input.value;
    for (var i = 0; i < dsnvArr.length; i++) {

        if (dsnvArr[i].loaiNhanVien().includes(value)) {
            arrSearch.push(dsnvArr[i]);
        }


    }
    // console.log('arrSearch: ', arrSearch);
    // render ra table
    inDSNV(arrSearch);

    }
