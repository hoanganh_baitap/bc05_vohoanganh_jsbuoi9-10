
var validator = {
    kiemTraTrung: function(id, dsnv, message){
        var viTri = timViTri(id, dsnv);
        if(viTri == -1){
            thongBao("tbTKNV", "");
            return true;
        }else{
            thongBao("tbTKNV", "Tài khoản bị trùng");
            return false;
        }
    },
    kiemTraRong: function(value, id, message){
        if(value.length == 0){
            thongBao(id, message);
            return false;
        }else{
            thongBao(id, "");
            return true;
        }
    },
    kiemTraDoDai: function(value, id, message, min, max){
        if(value.length < min || value.length > max){
            thongBao(id, message);
            return false;
        }else{
            thongBao(id, "");
            return true;
        }
    },
    kiemTraLaChuSo: function(value, id, message){
        var regex = /^([^0-9]*)$/;
        if (regex.test(value)) {
            
            thongBao(id, "");
            return true;
        }
         else {
            thongBao(id, message);
            return false;
        }
    },
    kiemTraEmail: function(value, id, mesage){
        const re =
            /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
            if(re.test(value)){
                thongBao(id, "");
                return true;
            }else{
                thongBao(id, mesage);
                return false;
            }
    },
    kiemTraMatKhau: function(value, id, message){
        var re = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/;
        if(re.test(value)){
            thongBao(id, "");
            return true;
        }else{
            thongBao(id, message);
            return false;
        }
    },
    kiemTraNgay: function(value, id, message){
        var re = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
        if(re.test(value)){
            thongBao(id, "");
            return true;
        }else{
            thongBao(id, message);
            return false;
        }
    },
    kiemTraMinMax: function(value, id, message, min, max){
        if(value *1 < min || value*1 > max){
            thongBao(id, message);
            return false;
        }else{
            thongBao(id, "");
            return true;
        }
    },
    kiemTraChucVu: function(value, id, message){
        if(value == "Chọn chức vụ" || value == ""){
            thongBao(id, message);
            return false;
        }else{
            thongBao(id, "");
            return true;
        }
    }
}

// function thong bao loi dinh dang
function thongBao(id, message){
    document.getElementById(id).innerText = message;
}