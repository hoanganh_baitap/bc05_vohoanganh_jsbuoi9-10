
// ham lay thong tin nhan vien
function layThongTin() {
    var taiKhoan = document.getElementById("tknv").value;
    var hoTen = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var matKhau = document.getElementById("password").value;
    var ngayLam = document.getElementById("datepicker").value;
    var luongCB = document.getElementById("luongCB").value;
    var chucVu = document.getElementById("chucvu").value;
    var gioLam = document.getElementById("gioLam").value;

    return new NhanVien(taiKhoan, hoTen, email, matKhau, ngayLam, luongCB, chucVu, gioLam);
}

// ham in danh sach nhan viên lên giao diện
function inDSNV(dsnv){
    var inDanhSach = "";
    // duyetj mảng
    for(var i = 0; i < dsnv.length; i++){
        var nv = dsnv[i];
        var inNhanVien = `<tr>
        <td>${nv.taiKhoan}</td>
        <td>${nv.ten}</td>
        <td>${nv.email}</td>
        <td>${nv.ngay}</td>
        <td>${nv.chuc}</td>
        <td>${nv.tongLuong()}</td>
        <td>${nv.loaiNhanVien()}</td>
        <td>
        <button data-toggle="modal" data-target="#myModal" class="btn btn-warning mx-2 my-2" onclick="suaNV('${nv.taiKhoan}')">Sửa</button>
        <button class="btn btn-danger" onclick="xoaNV('${nv.taiKhoan}')">Xóa</button>

        </td>
        </tr>`
        inDanhSach+= inNhanVien;
    }
    // show lên giao diện
    document.getElementById("tableDanhSach").innerHTML = inDanhSach;
}


// hàm tìm vị trí nhân viên theo tài khoản
function timViTri(taiKhoan, dsnv){
    for(var i = 0; i < dsnv.length; i++){
        var nv = dsnv[i];
        if(nv.taiKhoan == taiKhoan){
            return i;
        }
    }
    return -1;
}

// hàm xuất thông tin nhân viên
function xuatThongTin(nv){
    document.getElementById("tknv").value= nv.taiKhoan;
    document.getElementById("name").value= nv.ten;
    document.getElementById("email").value= nv.email;
    document.getElementById("password").value= nv.pass;
    document.getElementById("datepicker").value= nv.ngay;
    document.getElementById("luongCB").value= nv.luong;
    document.getElementById("chucvu").value= nv.chuc;
    document.getElementById("gioLam").value= nv.gio;

}
