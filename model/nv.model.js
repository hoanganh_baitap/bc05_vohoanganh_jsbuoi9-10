function NhanVien(taiKhoan, hoTen, email, matKhau, ngayLam, luongCB, chucVu, gioLam){
    this.taiKhoan = taiKhoan,
    this.ten = hoTen,
    this.email = email,
    this.pass = matKhau,
    this.ngay = ngayLam,
    this.gio = gioLam,
    this.chuc = chucVu,
    this.luong = luongCB
    this.tongLuong = function(){
        if(this.chuc == "Sếp"){
            return (this.luong*1 *3)
        }
        if(this.chuc == "Trưởng phòng"){
            return (this.luong*1 *2);
        }
        if(this.chuc == "Nhân viên"){
            return (this.luong*1)
        }
    },
    this.loaiNhanVien = function(){
        if(this.gio >= 192){
            return "Nhân viên xuất sắc"
        }else if(this.gio >=176){
            return "Nhân viên giỏi"
        }else if(this.gio >= 160){
            return "Nhân viên khá"
        }else{
            return "Nhân viên trung bình"
        }
    }
}